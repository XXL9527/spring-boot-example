package com.example.demo;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.bind.annotation.RestController;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2 // Swagger(2.0)を有効にする
public class SwaggerConfig {

	@Bean
    public Docket swaggerSpringMvcPlugin() {
        return new Docket(DocumentationType.SWAGGER_2)
        				.apiInfo(apiInfo())
                        .select()
                        .paths(PathSelectors.any())
                        .apis(RequestHandlerSelectors.basePackage("com.example.demo.controller"))		// root package
                        .apis(RequestHandlerSelectors.withClassAnnotation(RestController.class))		// 対象クラスのアノテーションタイプ
                        .build() ;
    }

    private ApiInfo apiInfo() {
    	
    	ApiInfoBuilder builder = new ApiInfoBuilder();
    	builder.title("Springboot Rest Swagger Example");
    	builder.description("Web API 仕様書");
    	builder.version("1.0");
    	builder.termsOfServiceUrl("https://www.example.org");
    	builder.contact(new Contact("XXL9527", "/demo/", "song.yanxin@mail.com"));
    	builder.license("Apache 2.0");
    	builder.licenseUrl("http://www.apache.org/licenses/LICENSE-2.0.html");
    	
        return builder.build();
    }
}
