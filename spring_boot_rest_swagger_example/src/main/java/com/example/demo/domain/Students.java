package com.example.demo.domain;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Swagger アノテーションによるAPI Docs仕様
 * @author song.yanxin
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "生徒オブジェクト",description="") 
public class Students {

	@ApiModelProperty(value = "生徒番号", required=true)
	private String stuId;
	@ApiModelProperty(value = "生徒名前", required=true)
	private String stuNm;
	@ApiModelProperty(value = "生徒性別")
	private String sex;
	@ApiModelProperty(value = "生徒年齢")
	private int age;
}
