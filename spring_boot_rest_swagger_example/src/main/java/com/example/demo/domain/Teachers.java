package com.example.demo.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Swagger デフォルトAPI Docs仕様
 * @author song.yanxin
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Teachers {

	private String tchrId;
	private String tchrNm;
	private String sex;
	private int age;
}
