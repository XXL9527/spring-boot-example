package com.example.demo.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.domain.Students;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

/**
 * Swagger アノテーションによるAPI Docs仕様
 * @author song.yanxin
 */
@RestController
@RequestMapping("api/v1")
@Api(tags = "生徒モジュール")
public class StudentController {

	@RequestMapping(path = "/getStudent", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiOperation(value="生徒情報取得", notes="詳細情報を取得する", httpMethod="GET")
	@ResponseBody
	public Students getStudent(
			@RequestParam(required=true) 
			@ApiParam(value = "生徒番号", required=true)
			String id
	) {
		Random random = new Random();
		return new Students(id, "生徒_" + id, random.nextBoolean()? "男" : "女", random.nextInt(12) + 10);
	}

	@RequestMapping(path = "/getStudentList", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiOperation(value="生徒リスト取得", notes="リストデータを取得する", httpMethod="POST")
	@ResponseBody
	public List<Students> getStudentList(Students srchData) {

		Random random = new Random();
		int length = random.nextInt(9);

		List<Students> rsltList = new ArrayList<>();
		for (int i = 1; i < length; i++) {
			rsltList.add(new Students(String.format("%04d",i), "生徒_" + i, random.nextBoolean() ? "男" : "女", random.nextInt(12) + 10));
		}
		return rsltList;
	}

	@RequestMapping(path = "/addStudent", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiOperation(value="生徒新規作成", notes="生徒情報を新規作成する", httpMethod="PUT")
	public boolean addStudent(Students data) {
		// do something
		Random random = new Random();
		return random.nextBoolean();
	}

	@RequestMapping(path = "/updStudent", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiOperation(value="生徒情報更新", notes="生徒情報を更新/変更する", httpMethod="POST")
	public boolean updStudent(Students data) {
		// do something
		Random random = new Random();
		return random.nextBoolean();
	}
	
	@RequestMapping(path = "/deleteStudent", method = RequestMethod.DELETE)
	@ApiOperation(value="生徒情報削除", notes="生徒情報を削除する", httpMethod="DELETE", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public boolean delStudent(
			@RequestParam(required=true) 
			@ApiParam(value = "生徒番号", required=true)
			String id
	) {
		// do something
		Random random = new Random();
		return random.nextBoolean();
	}
}
