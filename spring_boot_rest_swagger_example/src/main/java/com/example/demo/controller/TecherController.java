package com.example.demo.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.domain.Teachers;

/**
 * Swagger デフォルトAPI Docs仕様
 * @author song.yanxin
 */
@RestController
@RequestMapping("api/v1")
public class TecherController {

	@RequestMapping(path = "/getTeacher", method = RequestMethod.GET)
	public Teachers getTeacher(String id) {

		Random random = new Random();
		return new Teachers(id, "教師_" + id, random.nextBoolean()? "男" : "女", random.nextInt(12) + 10);
	}

	@RequestMapping(path = "/getTeacherList", method = RequestMethod.POST)
	public List<Teachers> getTeacherList(Teachers srchData) {

		Random random = new Random();
		int length = random.nextInt(9);

		List<Teachers> rsltList = new ArrayList<>();
		for (int i = 1; i < length; i++) {
			rsltList.add(new Teachers(String.format("%04d",i), "教師_" + i, random.nextBoolean() ? "男" : "女", random.nextInt(12) + 10));
		}
		return rsltList;
	}

	@RequestMapping(path = "/addTeacher", method = RequestMethod.PUT)
	public boolean addTeacher(Teachers data) {
		// do something
		Random random = new Random();
		return random.nextBoolean();
	}

	@RequestMapping(path = "/updTeacher", method = RequestMethod.POST)
	public boolean updTeacher(Teachers data) {
		// do something
		Random random = new Random();
		return random.nextBoolean();
	}
	
	@RequestMapping(path = "/deleteTeacher", method = RequestMethod.DELETE)
	public boolean delTeacher(String id) {
		// do something
		Random random = new Random();
		return random.nextBoolean();
	}
}
